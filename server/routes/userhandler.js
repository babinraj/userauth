const Users = require("../models/users");

const encryptionPasscode = require("./Encryption/encryption");

module.exports = {
    addUser: (reqBody) => {
        return new Promise((resolve, reject) => {
            const { name, email, password } = reqBody;
            encryptionPasscode.encrypt(password).then((resEncrpt) => {
                const userInstance = new Users();
                userInstance.name = name;
                userInstance.email = email;
                userInstance.password = resEncrpt;
                userInstance.save((err, data) => {
                    if (err) {
                        resolve(err, null);
                    }
                    else {
                        resolve(null, "success");
                    }
                })
            })
        });
    },
    getUser: (userCred) => {
        return new Promise((resolve, reject) => {
            console.log("userCred", userCred);
            if (userCred) {
                Users.findOne({
                    email: userCred.email
                }).exec(function(error, result) {
                    if (error) {
                      reject(error);
                    }
                    resolve(result);
                });
            } else {
                resolve("error", null);
            }

        })
    }
}