/* eslint-disable @typescript-eslint/no-var-requires */
const crypto = require("crypto");
const encryptConfig = require("./encryptconfig.json");
module.exports = {
  encrypt: (text, privatekey = null) => {
    return new Promise((resolve) => {
      let key;
      if (privatekey) {
        key = privatekey;
      } else {
        key = crypto.randomBytes(32);
      }
      const cipher = crypto.createCipheriv(
        encryptConfig.algorithm,
        Buffer.from(key, "hex"),
        Buffer.from(encryptConfig.initial_vector, "hex")
      );
      let encrypted = cipher.update(text);
      encrypted = Buffer.concat([encrypted, cipher.final()]);
      const encryptObj = {
        passwordkey: key.toString("hex"),
        passwordvalue: encrypted.toString("hex")
      };
      resolve(encryptObj);
    });
  }
};
