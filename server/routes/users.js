var express = require('express');
var router = express.Router();
// const jwt = require('jsonwebtoken');
// const jwtKey = "97d91f6d5baefd4108cbabf2611c8e3f622c30c37d48a2efb6d59056f741eb88";
// // var redisClient = require("../controller/redis-server");
// const redis = require("redis");
// // var session = require('express-session');
// var RedisClient = redis.createClient();
var userModal = require("../models/users");

var userHandler = require("./userhandler");
const AuthController = require('../controller/auth-controllers')

const encryptionPasscode = require("./Encryption/encryption");

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.send('respond with a resource');
});

router.post('/register', AuthController.register)
router.post('/authlogin', AuthController.login)
router.delete('/logout', AuthController.logout)

router.get("/getUserDetails", function (req, res) {
  userModal.find({}, function (err, result) {
    if (err)
      res.send({ message: "User Not found!", status: 500 });
    else {
      const userData = result
      res.send({ 
        status: 200,
        responseArray: { userData: userData }
      });
    }
  })

})

module.exports = router;
