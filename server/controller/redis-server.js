const redis = require("redis");
var session = require('express-session');
var redisStore = require('connect-redis')(session);
var RedisClient = redis.createClient();
const secret =
    "auth-login-nOhl2wQULx-tiSmSRdRqc-XcXaf8APYg-vDAR4FkJtF-VyZrEhPhff";
module.exports = (app) => {

    app.use(session({
        secret: secret,
        store: new redisStore({ client: RedisClient, ttl: 260 }),
        saveUninitialized: false,
        resave: false,
        cookie: { secure: true }
    }));

    RedisClient.on('error', function (err) {
        console.log('Redis error: ' + err);
    });

    RedisClient.on("ready", function () {
        console.log("Redis is ready");
    });

}